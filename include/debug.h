#ifndef _DEBUG_H_
#define _DEBUG_H_

//#define CONFIG_DEBUG 1
#ifdef CONFIG_DEBUG
	#define dbg_printk(fmt, s...)	printk("%s,%d: " fmt, __FUNCTION__, __LINE__, ##s)
#else
	#define dbg_printk(fmt, s...)
#endif

#endif
