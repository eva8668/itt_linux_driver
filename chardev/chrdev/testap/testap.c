#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int fd;
	int n;
	char buf[128];

	printf("Start to open the device node.\n");
	fd = open("/dev/ex3", O_RDWR);
	printf("Open device node OK. Get fd = %d\n", fd);
    	n=sprintf(buf,"%s","hello world");

	printf("write-->%s\n",buf);
	n=write(fd,buf,n);
	
    	n=read(fd,buf,128);
    	buf[n]=0;   
	printf("Read <--n=%d,%s\n\n",n,buf);

	close(fd);
	return 0;
}
