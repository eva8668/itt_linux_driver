
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/spinlock.h>
#include <linux/mutex.h>
#include <linux/tty.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

#define CONFIG_DEBUG
#include "../../include/debug.h"

#define MY_MAJOR	40



static loff_t	my_lseek(struct file *file, loff_t offset, int orig)
{
	dbg_printk("\n");
	return file->f_pos;
}

static ssize_t	my_read(struct file *file, char __user *buf, size_t count, loff_t *ppos)
{
	char	*kbuf;
        kbuf=file->private_data;
	dbg_printk("count=%d\n", count);
	int n;
#if 0
	kbuf = kmalloc(count+2, GFP_KERNEL);
	if ( kbuf == NULL ) {
		printk("Kernel memory allocate failed !\n");
		return 0;
	}
	memset(kbuf, 'a', count);

	kbuf[count] = 0;
	kbuf[count-1] ='\n' ;	// EOF code

	
#endif

	n=strlen(kbuf);
	if ( copy_to_user(buf, kbuf,n) ) { 
		dbg_printk("Copy data to user context fail !\n");
		kfree(kbuf);
		return 0;
         }
//	kfree(kbuf);
//	return count;
	return n;



}

static ssize_t	my_write(struct file *file, const char __user *buf, size_t count, loff_t *ppos)
{

     
	char	*kbuf=(char *)file->private_data;

#if 0
	kbuf = kmalloc(count+1, GFP_KERNEL);
	if ( kbuf == NULL ) {
		printk("Kernel memory allocate failed !\n");
		return 0;
	}
#endif	
	if ( copy_from_user(kbuf, buf, count) ) {
		dbg_printk("Get data from user failed !\n");
		//kfree(kbuf);
		return 0;
	}
	kbuf[count] = 0;
	dbg_printk("Write data : %s\n", kbuf);
//	kfree(kbuf);
	return count;
}

static int	my_open(struct inode *inode, struct file *file)
{
	int	minor=iminor(inode);
          file->private_data = kmalloc(100, GFP_KERNEL);
     if ( file->private_data == NULL ) {
		 printk("Kernel memory allocate failed !\n");
		 return 0;
	  }
	memset(file->private_data,0,100);

	dbg_printk("minor=%d\n", minor);
	return 0;
}

static long	my_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	dbg_printk("\n");
	return 0;
}

static int	my_release(struct inode *inode, struct file *file)
{
	dbg_printk("\n");
	kfree(file->private_data);
	return 0;
}

static const struct file_operations	my_fops = {
	.llseek		= my_lseek,
	.read		= my_read,
	.write		= my_write,
	.open		= my_open,
	.unlocked_ioctl	= my_ioctl,
	.release	= my_release,
};

static int	__init ex3_init(void)
{
	dbg_printk(" Regist ex3\n");
	return register_chrdev(MY_MAJOR, "ex3", &my_fops);
}

static void	__exit ex3_exit(void)
{
	dbg_printk("Unregist ex3\n");
	unregister_chrdev(MY_MAJOR, "ex3");
}

module_init(ex3_init);
module_exit(ex3_exit);

MODULE_LICENSE("GNU");
