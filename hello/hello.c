#include <linux/module.h>
#define CONFIG_DEBUG
#include "../include/debug.h"

static int __init hello_init(void)
{
	dbg_printk("hello init\n");
	return 0;
}

static void  __exit hello_exit(void)
{
	dbg_printk("hello exit\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GNU");
