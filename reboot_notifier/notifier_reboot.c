#include <linux/init.h>
#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/notifier.h>
#include <linux/delay.h>
MODULE_LICENSE("GPL");
static int test_reboot(struct notifier_block *n, unsigned long code, void *p)
{
        if(code != SYS_RESTART)
	{	
		while(1)
		printk(KERN_EMERG"not reboot\n");
                return NOTIFY_DONE;
	}
	while(1)
        printk(KERN_EMERG "notify reboot\n");
	
        return NOTIFY_DONE;
}
static struct notifier_block mtd_notifier = {
	.notifier_call =    test_reboot,
};


static int __init init_modules(void)
{
    
    register_reboot_notifier(&mtd_notifier);
	printk(KERN_EMERG "reboot notifier register\n");
    return 0;
}


static int __exit exit_modules(void)
{
    
	printk(KERN_EMERG "exit reboot notifier register\n");
    return 0;
}
module_init(init_modules);
module_exit(exit_modules);

