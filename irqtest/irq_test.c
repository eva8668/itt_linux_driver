
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/fs.h>
#include <linux/debugfs.h>
#include <linux/mm.h>  		
#include <linux/slab.h>  

#define GPIO_INT 193
#define KSWITCH_GPIO_PIN 23
int		 irq_number;

static irqreturn_t GPIO_INTerrupt(int irq, void* dev_id){

	printk("GPIO_INTerrupt\n");
	return(IRQ_HANDLED);
}

static int __init irq_test_init(void) {
		//irq_number = gpio_to_irq(23);
		//printk("irq_number=%d\n",irq_number);
//
	if ( request_irq(GPIO_INT, GPIO_INTerrupt, IRQF_TRIGGER_RISING|IRQF_ONESHOT, "GPIO_INT", NULL) ) {
		printk(KERN_ERR "eroor: requesting IRQ %d",GPIO_INT);
		return(-EIO);
	} else {
		printk(KERN_ERR "requesting IRQ %d-> ok\n",GPIO_INT);
	}
	
	return 0;
}

static void __exit irq_test_exit(void) {
	free_irq(GPIO_INT, NULL);
	printk (" irq test exit\n");
	return;
}

module_init(irq_test_init);
module_exit(irq_test_exit);

MODULE_LICENSE("GPL");

