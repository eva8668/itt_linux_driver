#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fcntl.h>
#include <linux/delay.h>
#include <linux/timer.h>
#include <linux/sched.h>
#include <linux/mm.h>
#include <linux/kmod.h>
#include <linux/workqueue.h>
#include <linux/kthread.h>

#define CONFIG_DEBUG
#include "../include/debug.h"

#define TIMER_INTERVAL		(3*HZ)	// 3 seconds
static struct timer_list        polltimer;
static struct task_struct	*waitkthread;
static wait_queue_head_t	waitqueue;
static int			exitflag=0;

static int waitqueue_kthread(void *data)
{
	dbg_printk("\n");
	while ( exitflag == 0 ) {
		interruptible_sleep_on(&waitqueue);
		dbg_printk("%s be waken up \n",__func__);
	}
	dbg_printk("\n");
	return 0;
}

static void     my_poll(unsigned long noused)
{
        dbg_printk("\n");
	if ( exitflag )
		return;
	wake_up_interruptible(&waitqueue);
        polltimer.expires = jiffies + TIMER_INTERVAL;
        add_timer(&polltimer);
}

static void __exit waitqueue_exit(void)
{
	dbg_printk("\n");
	exitflag = 1;
	del_timer(&polltimer);
	wake_up_interruptible(&waitqueue);
	kthread_stop(waitkthread);
}

static int __init waitqueue_init(void)
{
	dbg_printk("\n");
	init_waitqueue_head(&waitqueue);

	waitkthread = kthread_run(waitqueue_kthread, NULL, "it-waitqueue");
	if ( IS_ERR(waitkthread) ) {
		dbg_printk("Create kernel thread fail !\n");
		return -1;
	}
	dbg_printk("\n");

        // initialize polling timer
        init_timer(&polltimer);
        polltimer.function = my_poll;
        polltimer.expires = jiffies + TIMER_INTERVAL;
        add_timer(&polltimer);

	return 0;
}

module_init(waitqueue_init);
module_exit(waitqueue_exit);

MODULE_AUTHOR("Ittraining");
MODULE_LICENSE("GPL");
