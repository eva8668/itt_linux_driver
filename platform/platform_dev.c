#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include "test_platform.h"

MODULE_LICENSE("GPL");

static struct resource test_resources[] = {
        {
                .start          = RESOURCE_START_ADDRESS,
                .end            = RESOURCE_END_ADDRESS,
                .flags          = IORESOURCE_MEM,
        },
};


static struct platform_device test_device = {
        .name           = DRIVER_NAME,
        .id             = -1,
        .num_resources  = ARRAY_SIZE(test_resources),
        .resource       = test_resources,
};

int platform_dev_init(void)
{
	printk("platform device init\n");
	platform_device_register(&test_device);
	return 0;
}

void platform_dev_exit(void)
{
	platform_device_unregister(&test_device);
	printk("platform device exit\n");
}

module_init(platform_dev_init);
module_exit(platform_dev_exit);
