#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include "test_platform.h"

MODULE_LICENSE("GPL");

static int test_drv_probe(struct platform_device *pdev)
{

 	struct resource *res;
	printk("test probe\n");
        res = platform_get_resource(pdev, IORESOURCE_MEM, 1);
        if (unlikely(!res)) {
                return -1;
        }

	printk(KERN_ALERT "\n IRQ Num:%d\n", platform_get_irq(pdev, 0));
 	return 0;  
}

static int test_drv_remove(struct platform_device *pdev)
{
	return 0;
}

static struct platform_driver test_pldriver = {
	.probe          = test_drv_probe,
	.remove         = test_drv_remove,
	.driver = {
			.name  = DRIVER_NAME,
	},
};

int platform_drv_init(void)
{
	printk("platform drv init\n");
	platform_driver_register(&test_pldriver);
	return 0;
}

void platform_drv_exit(void)
{
	platform_driver_unregister(&test_pldriver);
	printk("platform_drv exit\n");
	return;
}

module_init(platform_drv_init);
module_exit(platform_drv_exit);

