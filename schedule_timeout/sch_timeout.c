#include <linux/init.h>
#include <linux/module.h>
#include <linux/kthread.h>

MODULE_LICENSE("GPL");

struct task_struct *kth_sch_tsk;
int data;

static int kth_sch(void *arg)
{
    unsigned int timeout;
    int *d = (int *) arg;
    
    for(;;) {
        if (kthread_should_stop()) break;
        printk("%s(): %d\n", __FUNCTION__, (*d)++);
        do {
            set_current_state(TASK_INTERRUPTIBLE);
            timeout = schedule_timeout(2 * HZ);
        } while(timeout);
    }
    printk("exit kth_sch\n");

    return 0;
}

static int __init init_modules(void)
{
    int ret;

    kth_sch_tsk = kthread_create(kth_sch, &data, "kth_sch");
    if (IS_ERR(kth_sch_tsk)) {
        ret = PTR_ERR(kth_sch_tsk);
        kth_sch_tsk = NULL;
        goto out;
    }
    wake_up_process(kth_sch_tsk);

    return 0;

out:
    return ret;
}

static void __exit exit_modules(void)
{
    kthread_stop(kth_sch_tsk);
}

module_init(init_modules);
module_exit(exit_modules);
