
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/spinlock.h>
#include <linux/mutex.h>
#include <linux/tty.h>
#include <linux/proc_fs.h>
#include <linux/timer.h>
#include <linux/sysfs.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <asm/uaccess.h>

#define CONFIG_DEBUG
#include "../include/debug.h"

#define MY_MAJOR	40

struct class	*sysfs_class;

static ssize_t
sysfs_show_name(struct device *dev, struct device_attribute *attr, char *buf)
{
        return sprintf(buf, "show name\n");
}

static ssize_t
sysfs_show_date(struct device *dev, struct device_attribute *attr, char *buf)
{
        ssize_t retval;

	retval = sprintf(buf, "show date\n");

	return retval;
}

static ssize_t
sysfs_show_time(struct device *dev, struct device_attribute *attr, char *buf)
{
        ssize_t retval;

	retval = sprintf(buf, "show time\n");

        return retval;
}

static ssize_t
sysfs_show_max_user_freq(struct device *dev, struct device_attribute *attr, char *buf)
{
        return sprintf(buf, "show max user freq\n");
}

static ssize_t
sysfs_set_max_user_freq(struct device *dev, struct device_attribute *attr, const char *buf, size_t n)
{
	printk("get string = %s\n", buf);

        return n;
}

static struct device_attribute sysfs_attrs[] = {
        __ATTR(name, S_IRUGO, sysfs_show_name, NULL),
        __ATTR(date, S_IRUGO, sysfs_show_date, NULL),
        __ATTR(time, S_IRUGO, sysfs_show_time, NULL),
        __ATTR(max_user_freq, S_IRUGO | S_IWUSR, sysfs_show_max_user_freq, sysfs_set_max_user_freq),
        { },
};

static int sysfs_suspend(struct device *dev, pm_message_t mesg)
{
	dbg_printk("\n");
	return 0;
}

static int sysfs_resume(struct device *dev)
{
	dbg_printk("\n");
	return 0;
}

static int	__init mysysfs_init(void)
{
	dbg_printk("\n");
	sysfs_class = class_create(THIS_MODULE, "mysysfs");
        if (IS_ERR(sysfs_class)) {
                printk(KERN_ERR "%s: couldn't create class\n", __FILE__);
                return PTR_ERR(sysfs_class);
        }
	dbg_printk("class create OK.\n");

    sysfs_class->suspend = sysfs_suspend;
    sysfs_class->resume = sysfs_resume;
	sysfs_class->dev_attrs = sysfs_attrs;

	device_create(sysfs_class, NULL, MKDEV(200, 0), NULL, "mysysfs0");

	return 0;
}

static void	__exit mysysfs_exit(void)
{
	dbg_printk("\n");
	device_destroy(sysfs_class, MKDEV(200, 0));
	class_destroy(sysfs_class);
}

module_init(mysysfs_init);
module_exit(mysysfs_exit);

MODULE_LICENSE("GPL");
