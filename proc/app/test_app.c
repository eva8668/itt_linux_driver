#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>

int main()
{
	int fd;
	char buf[128];
 	int r;
 	int i=0;
 	
	fd=open("/proc/driver/myproc",O_RDWR);

 	while( ( r = read(fd,buf+i,1) ) > 0 ) 
		i++;
 
	buf[i]=0;
	printf("%s\n",buf);
	close(fd);
	return 0;
}

