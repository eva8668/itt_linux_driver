
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/spinlock.h>
#include <linux/mutex.h>
#include <linux/tty.h>
#include <linux/proc_fs.h>
#include <linux/timer.h>
#include <asm/uaccess.h>
#include <linux/slab.h>

#define CONFIG_DEBUG
#include "../include/debug.h"

#define MY_MAJOR	40
#define MY_PROC_NAME		"driver/myproc"

int len,tmp;
char *pdata;

static int my_read_proc(struct file *filp,char *buf,size_t count,loff_t *offp ) 
{
	
	if(count>tmp)
	{
		count=tmp;
	}
	tmp=tmp-count;
	
	if( copy_to_user(buf,pdata+(len-tmp-1), count) )
	{
		dbg_printk("my_read_proc error\n");
                return -EFAULT;
	}

	if(count==0)
		tmp=len;
   
	return count;
	
}

int my_write_proc(struct file *filp,const char *buf,size_t count,loff_t *offp)
{
	if( copy_from_user(pdata,buf,count) )
	{
		dbg_printk("my_write_proc error\n");
                return -EFAULT;
	}
	len=count;
	tmp=len;
	pdata[count] = 0;
	dbg_printk("string=%s\n", pdata);
	return count;
}

struct file_operations proc_fops = {
	read: my_read_proc,
	write: my_write_proc
};
static int __init proc_init(void)
{
	dbg_printk("\n");
	proc_create(MY_PROC_NAME,0,NULL,&proc_fops);
	pdata=kmalloc(1024,GFP_KERNEL);
	dbg_printk("proc init\n");
	return 0;
}

static void __exit proc_exit(void)
{
	dbg_printk("\n");
	kfree(pdata);
	remove_proc_entry(MY_PROC_NAME, 0);
}

module_init(proc_init);
module_exit(proc_exit);

MODULE_LICENSE("GNU");
