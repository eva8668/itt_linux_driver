#include <stdio.h>
#include <stdlib.h>

#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)

#define container_of(ptr, type, member) ({            \
 const typeof( ((type *)0)->member ) *__mptr = (ptr);    \
 (type *)( (char *)__mptr - offsetof(type,member) );})

struct st_data1 
{
	int a;
};

struct st_data2 
{
 	int b;
 	struct st_data1 z;
 	int c;
};

int main()
{
	struct st_data2 *obj;
 	struct st_data1 *obj1;
 	struct st_data2 *obj2;

 	obj = malloc(sizeof(struct st_data2));
 	if(obj == NULL){
       		printf("Error: Memory not allocated !\n");
 	}
	//initial data
 	obj->z.a = 10;
 	obj->b = 20;
 	obj->c = 30;
 
	//pointer to existing entry  
 	obj1 = &obj->z;
 	obj2 = container_of(obj1, struct st_data2, z);

 	printf("obj2->b = %d\n", obj2->b);

 	return 0;
}
