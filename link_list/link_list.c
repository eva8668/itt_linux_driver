#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/list.h>		/* For kernel Link list	*/
#include <linux/slab.h>		/* For kmalloc 		*/

MODULE_LICENSE("GPL");

/*
 * Dev structure
 */
struct Dev {
	char			dev_name[24];
	unsigned int		addr;		
	unsigned int		value;		
	struct list_head	list;		
};

struct Dev DevList;

static int linklist_init(void)
{
	struct Dev *aNewDev, *aDev;
	int i;

	printk(KERN_INFO "install link_list module\n");

	/* Create a list head*/
	INIT_LIST_HEAD(&DevList.list);

	/* Adding element to DevList */
	for (i = 0; i < 5; i++)
	{
		aNewDev = kmalloc(sizeof(struct Dev), GFP_KERNEL);
		sprintf(aNewDev->dev_name,"%s%d\n","i2c",i);
		aNewDev->addr =  i;
		aNewDev->value = i*10;
		INIT_LIST_HEAD(&aNewDev->list);

		/* Add the new node in DevList */
		list_add_tail(&(aNewDev->list), &(DevList.list));
	}

	printk(KERN_INFO "traversing the list using list_for_each_entry()\n");
	list_for_each_entry(aDev, &DevList.list, list)
	{
		printk(KERN_INFO "dev_name: %s, addr: %d, value: %d\n", aDev->dev_name, aDev->addr, aDev->value);
	}
	printk(KERN_INFO "\n");

	return 0;

} 

static void linklist_exit(void)
{
	struct Dev *aDev, *tmp;

	printk(KERN_INFO "exit linklist module.\n");

	list_for_each_entry_safe(aDev, tmp, &DevList.list, list)
	{
		printk(KERN_INFO "freeing node %s\n", aDev->dev_name);
		list_del(&aDev->list);
		kfree(aDev);
	}

} 

module_init(linklist_init);
module_exit(linklist_exit);
