
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kthread.h>
#include <linux/sched.h>

MODULE_LICENSE("Dual BSD/GPL");

static struct task_struct *kmain_task = NULL;
static wait_queue_head_t wait;

static int kth_fun(void *num)
{
	
	for (;;) {
		sleep_on_timeout(&wait, 3 * HZ);
	    	printk("%s called\n", __func__);
		if (kthread_should_stop())
			break;
	}

	printk("%s leaved\n", __func__);
	return 0;
}

static int kth_init(void)
{
	printk("install kth driver.\n");

	init_waitqueue_head(&wait);

	kmain_task = kthread_create(kth_fun, NULL, "kth_fun");
	if (IS_ERR(kmain_task)) {
		return PTR_ERR(kmain_task);
	}
	wake_up_process(kmain_task);

	return 0;
}

static void kth_exit(void)
{
	printk("remove kth driver\n");

	kthread_stop(kmain_task);
}

module_init(kth_init);
module_exit(kth_exit);

